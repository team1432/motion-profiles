import matplotlib.pyplot as plt
import numpy as np

acceleration_seconds = 2
deceleration_seconds = 1

max_fps = 8

distance_to_travel = -22

def intersection(line1, line2):
    x_diff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    y_diff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(x_diff, y_diff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, x_diff) / div
    y = det(d, y_diff) / div
    return x, y

def generate_trapezoid():
    sign = 1
    if distance_to_travel < 0:
        sign = -1
        distance = -distance_to_travel
    else:
        distance = distance_to_travel

    height = max_fps

    acceleration_triangle_width = acceleration_seconds
    deceleration_triangle_width = deceleration_seconds

    acceleration_triangle_area = acceleration_triangle_width * height / 2
    deceleration_triangle_area = deceleration_triangle_width * height / 2

    rectangle_area = distance - \
        acceleration_triangle_area - deceleration_triangle_area

    rectangle_width = rectangle_area / height

    bottom_left = (0, 0)
    top_left = (acceleration_triangle_width, height * sign)
    top_right = (acceleration_triangle_width + rectangle_width, height * sign)
    bottom_right = (acceleration_triangle_width +
                    rectangle_width + deceleration_triangle_width, 0)

    if acceleration_triangle_area + deceleration_triangle_area > distance:
        crossing_point = intersection((top_left, bottom_left), (top_right, bottom_right))
        top_left = crossing_point
        top_right = crossing_point

    return [bottom_left, top_left, top_right, bottom_right]

trapezoid = generate_trapezoid()
print(trapezoid)

def get_position(trapezoid, seconds):
    height = trapezoid[1][1]
    acceleration_triangle_area = trapezoid[1][0] * height / 2
    rectangle_area = height * (trapezoid[2][0] - trapezoid[1][0])
    deceleration_triangle_area = (trapezoid[3][0] - trapezoid[2][0]) * height / 2
    entire_trapezoid_area = acceleration_triangle_area + rectangle_area + deceleration_triangle_area

    # if it's in the first triangle
    if seconds < trapezoid[1][0]:
        slope = height / trapezoid[1][0]
        height = slope * seconds
        return height * seconds / 2

    # if it's in the rectangle
    elif seconds <= trapezoid[2][0]:
        width = seconds - trapezoid[1][0]
        return acceleration_triangle_area + height * width

    # it's in the last triangle
    elif seconds < trapezoid[3][0]:
        triangle_slope = height / (trapezoid[3][0] - trapezoid[2][0])
        missing_triangle_width = trapezoid[3][0] - seconds
        missing_triangle_height = missing_triangle_width * triangle_slope
        missing_triangle_area = missing_triangle_width * missing_triangle_height / 2

        return entire_trapezoid_area - missing_triangle_area

    # it's after the last triangle
    else:
        return entire_trapezoid_area

inputs = np.arange(0, 6, 0.01)
outputs = list(map(lambda x: get_position(trapezoid, x), inputs))

plt.plot(inputs, outputs)
plt.savefig('curve.png')

plt.plot(*zip(*trapezoid))
plt.savefig('trapezoid.png')

